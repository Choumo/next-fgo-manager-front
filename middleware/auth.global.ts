import {useToast} from "vue-toastification";

/**
 * Middleware that will check if user is authenticated and if his token is valid
 */
export default defineNuxtRouteMiddleware(async (to, from) => {
    const baseApiUrl = useRuntimeConfig().public.apiUrl;
    const toast = useToast();

    // We don't need to check anything on those routes
    if(to.path === "/" || to.path.startsWith("/multi-factor-authentication") || to.path === "/login" || to.path === "/privacy-policy" || to.path === "/register" || to.path.startsWith('/password-reset')) {
        return;
    }

    const cookie = useCookie('X-AUTH-TOKEN');

    // If no JWT token is present in the X-AUTH-TOKEN, we redirect to the homepage
    if(cookie.value === undefined || cookie.value === "" || cookie.value === null) {
        toast.error("You need to be authenticated to access this page.");
        return navigateTo('/');
    }

    // We fetch the middleware controller of the API
    const { error } = await useFetch(baseApiUrl + "middleware/", {
        method: "GET",
        server: false,
        headers: {
            "X-AUTH-TOKEN": cookie.value
        },
    });

    // If there is an error, token is not valid, so we remove the token from the cookies
    if(error.value) {
        cookie.value = null;
        toast.error("You can not connect to the server");
        return navigateTo('/');
    }
})
