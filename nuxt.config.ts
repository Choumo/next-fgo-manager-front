import path from 'path';
import daisyui from "daisyui";

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  devtools: { enabled: false },
  modules: ['@nuxtjs/tailwindcss', "@nuxt/image"],
  tailwindcss: {
    exposeConfig: true,
    viewer: true,
    config: {
      plugins: [
          require("daisyui"),
          require("@tailwindcss/typography")
      ],
      daisyui: {
        themes: [
          "light",
          "dark",
          "cupcake",
          "sunset",
          "black",
          "luxury",
          "night",
          "dracula",
          "halloween",
          {
            "next-fgo-manager": {
              "primary": "#3061b3",
              "secondary": "#00dfff",
              "accent": "#06b6d4",
              "neutral": "#f8fafc",
              "base-100": "#ffffff",
              "info": "#00b1f9",
              "success": "#22c55e",
              "warning": "#eab308",
              "error": "#dc2626",
              "bronze": "#ffa700"
            },
            "next-fgo-manager-dark": {
              "primary": "#3061b3",
              "secondary": "#00dfff",
              "accent": "#06b6d4",
              "neutral": "#14181c",
              "base-100": "#1e2328",
              "info": "#00b1f9",
              "success": "#22c55e",
              "warning": "#eab308",
              "error": "#dc2626",
              "bronze": "#ffa700"
            },
          },
        ],
      },
    },
  },
  runtimeConfig: {
    public: {
      apiUrl: process.env.API_URL
    }
  },
})