export async function afetch(url: string, method: string = "GET", body?: object): Promise<any> {
    const response: Response = await fetch("https://localhost/" + url, {
        method: method,
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
        mode: "cors",
    });

    if(!response.ok) {
        switch (response.status) {
            case 400:
                throw new Error('');
            case 401: /* Handle */ break;
            case 404: /* Handle */ break;
            case 500: /* Handle */ break;
        }
    }

    return await response.json();
}

export async function callApi(url: string, _requestMethod: string = "GET", body?: object) {
    const baseApiUrl = useRuntimeConfig().public.apiUrl;

    const { data, pending, error, refresh } = await useLazyFetch(baseApiUrl + url, {
        // @ts-ignore
        method: _requestMethod,
        body: body,
        server: false
    });

    return {
        data,
        pending,
        error,
        refresh
    };
}
