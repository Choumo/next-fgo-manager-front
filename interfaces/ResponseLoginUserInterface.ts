export interface ResponseLoginUserInterface {
    status: number,
    payload: {
        token: string
    }
}