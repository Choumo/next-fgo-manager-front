export interface ServantInformationInterface {
    id: number,
    name: string,
    class: string,
    rarity: number,
    graph: string,
    level: number,
    bond_level: number,
    np_level: number,
    servant_skill_1: {
        skill_level: number,
        skill_name: string,
        skill_icon: string
    },
    servant_skill_2: {
        skill_level: number,
        skill_name: string,
        skill_icon: string
    },
    servant_skill_3: {
        skill_level: number,
        skill_name: string,
        skill_icon: string
    },
    servant_append_1: {
        append_level: number,
        append_name: string,
        append_icon: string
    },
    servant_append_2: {
        append_level: number,
        append_name: string,
        append_icon: string
    },
    servant_append_3: {
        append_level: number,
        append_name: string,
        append_icon: string
    }
}