export interface CraftEssenceStatisticsInterface {
    totalCraftEssenceObtained: number,
    totalCraftEssenceUnobtained: number,
    totalCraftEssenceObtainedPercentage: number,
    craftEssenceRarityProgress: {
        fiveStars: {
            user: number,
            game: number
        },
        fourStars: {
            user: number,
            game: number
        },
        threeStars: {
            user: number,
            game: number
        },
        twoStars: {
            user: number,
            game: number
        },
        oneStars: {
            user: number,
            game: number
        },
    },
    craftEssenceTypeStatistics: {
        svtEquipFriendShip: number,
        svtEquipCampaign: number,
        svtEquipEvent: number,
        svtEquipEventReward: number,
        svtEquipManaExchange: number,
        normal: number,
        unknown: number,
    }
}