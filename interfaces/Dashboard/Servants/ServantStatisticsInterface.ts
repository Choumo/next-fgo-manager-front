export interface ServantStatisticsInterface {
    totalServantObtained: number,
    totalServantUnobtained: number,
    totalServantObtainedPercentage: number,
    servantRarityProgress: {
        fiveStars: {
            user: number,
            game: number
        },
        fourStars: {
            user: number,
            game: number
        },
        threeStars: {
            user: number,
            game: number
        },
        twoStars: {
            user: number,
            game: number
        },
        oneStars: {
            user: number,
            game: number
        },
    },
    servantClassStatistics: {
        saber: number,
        archer: number,
        lancer: number,
        rider: number,
        caster: number,
        assassin: number,
        berserker: number,
        ruler: number,
        avenger: number,
        moonCancer: number,
        alterEgo: number,
        foreigner: number,
        pretender: number,
        shielder: number,
        beast: number
    }
}