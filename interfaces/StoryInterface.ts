export interface StoryInterface {
    id: number,
    name: string,
    banner: string
}