export interface SpotCardInterface {
    id: number,
    name: string,
    image: string
}