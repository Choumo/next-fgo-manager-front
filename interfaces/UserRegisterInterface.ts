export interface UserRegisterInterface {
  email: string,
  password: string,
  passwordCheck: string
}