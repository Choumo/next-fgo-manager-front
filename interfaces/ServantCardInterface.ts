export interface ServantCardInterface {
    id: number,
    name: string,
    class: string,
    rarity: number,
    graph: string,
    isObtained: boolean
}