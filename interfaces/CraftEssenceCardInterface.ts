export interface CraftEssenceCardInterface {
    id: number,
    name: string,
    rarity: number,
    type: string,
    graph: string,
    isObtained: boolean
}