export interface ServantRessourcesInterface {
    id: number;
    name: string;
    class: string;
    graph: string;
    icon: string;
    rarity: number;
    ressources: {
        [id: number]: {
            id: number;
            name: string;
            background: string;
            icon: string;
            quantity: number;
        };
    };
}