export interface UserRessourcesInterface {
    ressources: {
        [id: number]: {
            id: number;
            name: string;
            background: string;
            icon: string;
            quantity: number;
        };
    };
}