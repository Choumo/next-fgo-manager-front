export interface TotalRessourcesInterface {
    ressources: {
        [id: number]: {
            id: number;
            name: string;
            background: string;
            icon: string;
            quantity: number;
        };
    };
}