import type {ServantRessourcesInterface} from "~/interfaces/Planner/Servant/ServantRessourcesInterface";
import type {UserRessourcesInterface} from "~/interfaces/Planner/User/UserRessourcesInterface";
import type {TotalRessourcesInterface} from "~/interfaces/Planner/Total/TotalRessourcesInterface";

export interface RessourcesNeededResponse {
    servants: {
        [servantId: string]: ServantRessourcesInterface
    };
    user: {
        [ressourceId: number]: UserRessourcesInterface
    };
    total: {
        [ressourceId: number]: TotalRessourcesInterface
    };
}