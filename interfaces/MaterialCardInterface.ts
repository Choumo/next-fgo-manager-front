export interface MaterialCardInterface {
    id: number,
    name: string,
    quantity: number,
    background: string,
    icon: string,
}
